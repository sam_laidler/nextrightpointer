package sam.laidler.next.right.pointer;

/**
 * https://leetcode.com/explore/challenge/card/november-leetcoding-challenge/565/week-2-november-8th-november-14th/3529/
 * 
 * @author samlaidler
 *
 */
public class NextRightPointer {
	public static void main(String [] args) {

		/*
		 * Recursion can give great functionality with small amounts of code, but it is
		 * tricky to grasp, at first.
		 * 
		 * The following steps may help.
		 */

		/*
		 * Step 1: choose a finite set of data and model it in a liner fashion (without
		 * recursion for now).
		 * 
		 * In this example I will go up to 7.
		 */
		
		Node root1 = new Node(1);
		
		root1.left = new Node(2);
		root1.right = new Node(3);
		root1.next = null;
		
		root1.left.left = new Node(4);
		root1.left.right = new Node(5);
		root1.left.next = new Node(3);
		
		root1.right.left = new Node(6);
		root1.right.right = new Node(7);
		root1.right.next = null;
		
		/*
		 * Step 2: rewrite the code with pointers and variables in such a way that
		 * patterns start to emerge.
		 */
		Node root2 = new Node();
		Node temp2 = root2;
		temp2.val = 1;
		
		// 1, 2, 3 (val, left, right)
		temp2.left = new Node(temp2.val * 2);
		temp2.right = new Node((temp2.val * 2) + 1);
		if (temp2.val % 2 == 1) { // true in this case
			temp2.next = null;
			temp2 = root2.left;
		}
		else {
			temp2.next = new Node(temp2.val + 1);
			temp2 = root2.right;
		}
		
		// 2, 4, 5
		temp2.left = new Node(temp2.val * 2);
		temp2.right = new Node((temp2.val * 2) + 1);
		if (temp2.val % 2 == 1) { // false in this case
			temp2.next = null;
			temp2 = root2.left;
		}
		else {
			temp2.next = new Node(temp2.val + 1);
			temp2 = root2.right;
		}
		
		// 3, 6, 7
		temp2.left = new Node(temp2.val * 2);
		temp2.right = new Node((temp2.val * 2) + 1);
		if (temp2.val % 2 == 1) { // false in this case
			temp2.next = null;
			temp2 = root2.left;
		}
		else {
			temp2.next = new Node(temp2.val + 1);
			temp2 = root2.right;
		}
		
		/*
		 * Step 3: rewrite the code so it fits in a loop.
		 * 
		 * Make the loop go round forever, using a break statement as required.
		 */
		Node root3 = new Node(1);
		Node temp3 = root3;
		int limit = 7;
		
		while (true) {
			temp3.left = new Node(temp3.val * 2);
			temp3.right = new Node((temp3.val * 2) + 1);
			/*
			 * This if block is one difference from the previous blocks of code (using
			 * root2).
			 */
			if (temp3.right.val == limit) {
				break;
			}
			else if (temp3.val % 2 == 1) {
				temp3.next = null;
				temp3 = root3.left;
			}
			else {
				temp3.next = new Node(temp3.val + 1);
				temp3 = root3.right;
			}
		}
		
		/*
		 * Step 4: encapsulate the code into a method. However, do NOT include the loop!
		 * 
		 * It is important to miss out the loop because the recursive nature of the
		 * function call will loop around the structure as required.
		 * 
		 * If all has gone well in the previous step, it should be theoretically
		 * possible to start by copying and pasting the code into the method. You then
		 * have to replace the break statement with a return statement that returns the
		 * root of the whole structure. Also replace, certain components with instance
		 * variables as appropriate, if they should not be reset with each iteration of
		 * the loop. Finally, set a return statement at the end which recursively calls
		 * the same function again.
		 */
		Node root4 = new Node(1);
		Solution s = new Solution();
		s.limit = 7;
		s.root = root4;
		root4 = s.connect(root4);
	}
}

//Definition for a Node.
class Node {
	public int val;
	public Node left;
	public Node right;
	public Node next;

	public Node() {
	}

	public Node(int _val) {
		val = _val;
	}

	public Node(int _val, Node _left, Node _right, Node _next) {
		val = _val;
		left = _left;
		right = _right;
		next = _next;
	}
}

class Solution {
	
	public Node root;
	public int limit;

	public Node connect(Node node) {
		node.left = new Node(node.val * 2);
		node.right = new Node((node.val * 2) + 1);
		if (node.right.val == limit) {
			return root;
		}
		else if (node.val % 2 == 1) {
			node.next = null;
			node = root.left;
		}
		else {
			node.next = new Node(node.val + 1);
			node = root.right;
		}
		return connect(node);
	}
}